
Known Limitations
=================

Approvers
~~~~~~~~~

When GPC add user as approver, this feature could have no effect on the
approvers list because the user is not an **eligible** approver for Gitlab
(`see documentation
<https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#eligible-approvers>`__).

A user can be added as an approver for a project if they are a member of:

- The project.
- The project’s immediate parent group.
- A group that has access to the project via share.

Label Management
~~~~~~~~~~~~~~~~

GPC is able to add or update labels on your projects,
but it won't remove existing labels which are not part of GPC configuration.

.. note::  This is more a "wanted" bahavior than a real known limitations.

  Labels can have many usage, so we let them be defined outside of GPC.

Badges
~~~~~~

Like labels, we do not change existing badges.
