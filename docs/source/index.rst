.. Gitlab-Project-Configurator documentation master file, created by
   sphinx-quickstart on Wed Jan 10 10:32:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gitlab Project Configurator's documentation!
=======================================================

GPC allows you to automatically configure one or several Gitlab
Project(s) settings.

.. image::  ../../data/gpc-icon.png

You can define your projects configuration in a versionned
configuration, following the "`Infrastructure as
Code <https://en.wikipedia.org/wiki/Infrastructure_as_code>`__\ "
principles.

GPC have multiple purposes, including:

- configure a huge number of Gitlab projects at once
- test if the desired change is supported by Gitlab before applying to the
  projects
- make sure that all your projects always have the right configuration
- ease some configuration (JIRA plugin or webhook configuration for example)
- renew / update some critical elements such as CI token for Artifactory or
  Gitlab
- allow derivation from the defined rules for any projects, all described in
  YAML allowing adding comments and traceabillity
- provide several ways of viewing the execution results, either by the stdout,
  an HTML report or a JSON file.

GPC acts like a "nice guy", it only changes some project settings you want to
enforce, without touching the others.
This allows a smooth transition between "everybody does what they want" and
"all projects should do this and that".

.. toctree::
    :maxdepth: 2
    :caption: Table of Contents
    :glob:

    readme
    usage
    config_files
    known_limitations
    releases
    cli
    reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
