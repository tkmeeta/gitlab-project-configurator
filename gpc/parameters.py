# coding: utf-8

"""
Command line parameters store.
"""

# Standard Library
from enum import Enum
from typing import Any  # pylint: disable=unused-import
from typing import List  # pylint: disable=unused-import
from typing import Optional  # pylint: disable=unused-import
from typing import Union  # pylint: disable=unused-import

# Third Party Libraries
import attr

from dictns import Namespace
from path import Path  # pylint: disable=unused-import


RawConfig = Namespace


class RunMode(Enum):

    DRY_RUN = "dry-run"
    APPLY = "apply"
    INTERACTIVE = "interactive"

    def __str__(self) -> str:
        return str(self.value)


@attr.s
class GpcParameters:
    config = attr.ib()  # type: Path
    force = attr.ib(default=False)  # type: bool
    # only = attr.ib(default=None)  # type: Optional[List[str]]
    mode = attr.ib(default=RunMode.DRY_RUN)  # type: RunMode
    projects = attr.ib(default=None)  # type: Optional[List[str]]
    report_file = attr.ib(default=None)  # type: Optional[str]
    report_html = attr.ib(default=None)  # type: Optional[str]
    diff = attr.ib(default=False)  # type: bool
    debug = attr.ib(default=False)  # type: bool
    config_project_url = attr.ib(default=None)  # type: Optional[str]
    gpc_enabled_badge_url = attr.ib(default=None)  # type: Optional[str]
    gpc_accepted_external_badge_image_urls = attr.ib(default=None)  # type: Optional[str]
    smtp_server = attr.ib(default=None)  # type: Optional[str]
    smtp_port = attr.ib(default=None)  # type: Optional[str]
    email_author = attr.ib(default=None)  # type: Optional[str]
    watchers = attr.ib(default=None)  # type: Optional[List[str]]
