# coding: utf-8

# Please keep __init__.py here to make this a directory a submodule
# to be used by importlib_resource

"""
This package is only used to provide the JSON Schema of the configuration file.
"""
