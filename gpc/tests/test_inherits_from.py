# coding: utf-8

"""
Test executor factory.
"""

# Third Party Libraries
import pytest

from dictns import Namespace

# Gitlab-Project-Configurator Modules
from gpc.rule_matcher import GroupRuleMatcher
from gpc.rule_matcher import ProjectRuleMatcher


# pylint: disable=redefined-outer-name, unused-argument, protected-access


@pytest.mark.parametrize(
    "rule_type,rule_object",
    [("projects_rules", ProjectRuleMatcher), ("groups_rules", GroupRuleMatcher)],
)
def test_inherits_from(rule_type, rule_object, mocker, fake_gitlab):

    raw_config = Namespace(
        {
            rule_type: [
                {
                    "rule_name": "rule1",
                    "mergerequests": {
                        "merge_method": "ff",
                        "printing_merge_request_link_enabled": False,
                    },
                    "permissions": {"visibility": "internal"},
                    "default_branch": "master",
                },
                {
                    "rule_name": "rule2",
                    "inherits_from": "rule1",
                    "mergerequests": {"merge_method": "merge"},
                    "permissions": {"visibility": "internal"},
                    "default_branch": "dev1",
                },
                {
                    "rule_name": "rule3",
                    "inherits_from": "rule2",
                    "mergerequests": {"merge_method": "rebase_merge"},
                    "default_branch": "rule/rev3",
                },
            ]
        }
    )

    rm = rule_object(gitlab=fake_gitlab, raw_config=raw_config)
    rule = Namespace(
        {
            "rule_name": "rule3",
            "inherits_from": "rule2",
            "mergerequests": {"merge_method": "rebase_merge"},
            "default_branch": "rule/rev3",
        }
    )
    # pylint: disable=protected-access
    assert rm._handle_rule_inheritance(rule) == {
        "mergerequests": {
            "merge_method": "rebase_merge",
            "printing_merge_request_link_enabled": False,
        },
        "permissions": {"visibility": "internal"},
        "default_branch": "rule/rev3",
        "inherits_from": "rule2",
        "rule_name": "rule3",
    }
