# coding: utf-8

"""
test_update project description
----------------------------------
"""
# Third Party Libraries
import pytest

from dictns import Namespace
from gitlab.v4.objects import Project  # pylint: disable=unused-import

# Gitlab-Project-Configurator Modules
from gpc.parameters import GpcParameters
from gpc.parameters import RunMode
from gpc.project_rule_executor import ProjectRuleExecutor


# pylint: disable=redefined-outer-name, unused-argument, protected-access


@pytest.mark.parametrize(
    "project_value, build_coverage_regex, expected_diff",
    [
        (
            None,
            "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
            {
                "property_name": "build_coverage_regex",
                "differences": {
                    "before": None,
                    "after": "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
                    "action": "added",
                },
            },
        ),
        (
            "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
            None,
            {
                "property_name": "build_coverage_regex",
                "differences": {
                    "before": "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
                    "after": None,
                    "action": "removed",
                },
            },
        ),
        (
            "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
            "^COVERAGE\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
            {
                "property_name": "build_coverage_regex",
                "differences": {
                    "before": "^TOTAL\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
                    "after": "^COVERAGE\\s+(?:\\d+\\s+){3,4}(\\d+\\%)$",
                    "action": "updated",
                },
            },
        ),
    ],
)
def test_update_project_pipeline_canceled(
    mocker,
    fake_gitlab,
    fake_project,
    project_value,
    build_coverage_regex,
    expected_diff,
):
    # Mock
    mocker.patch("gpc.tests.test_project_description.Project.save")
    mocker.patch(
        "gpc.tests.test_project_description.ProjectRuleExecutor.project",
        mocker.PropertyMock(return_value=fake_project),
    )
    fake_project.build_coverage_regex = project_value
    project_rules = Namespace(
        {
            "default_branch": "master",
            "build_coverage_regex": build_coverage_regex,
        }
    )
    p = ProjectRuleExecutor(
        gl=fake_gitlab,
        project_path="fake/path/to/project",
        rule=project_rules,
        gpc_params=GpcParameters(config=mocker.Mock("fake_config"), mode=RunMode.APPLY),
    )
    p.update_settings()

    assert p.get_changes_json() == [
        expected_diff,
        {
            "property_name": "default_branch",
            "differences": {
                "before": "old_default_branch",
                "after": "master",
                "action": "updated",
            },
        },
    ]
