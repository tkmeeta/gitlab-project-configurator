# coding: utf-8

"""
Apply rules on a Gitlab Project.
"""

# Standard Library
from abc import ABC
from abc import abstractmethod
from typing import Dict  # pylint: disable=unused-import
from typing import List  # pylint: disable=unused-import

# Third Party Libraries
import click

from boltons.cacheutils import cachedproperty
from gitlab import Gitlab
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.change_executors_factory import ChangeExecutorsFactory
from gpc.change_setting import ChangeSetting
from gpc.executors.change_executor import ChangeExecutor
from gpc.helpers.types import GroupRule
from gpc.helpers.types import ProjectName
from gpc.helpers.types import ProjectRule
from gpc.parameters import GpcParameters
from gpc.parameters import RunMode


log = get_logger()
SUCCESS = "success"
FAIL = "fail"


class RuleExecutor(ABC):

    """
    Based class to preprocess set of rules for given project or group.

    I basically receive a connection to the gitlab server, a json chunk that
    represent which rules to apply and one project path, and I am responsible for
    doing all calls to the Gitlab API to apply the changes.

    I support the 3 execution modes: dry run, interactive, apply.
    """

    def __init__(
        self,
        gl: Gitlab,
        rule: ProjectRule,
        gpc_params: GpcParameters,
    ):
        self.gitlab = gl
        self.rule = rule
        self.gpc_params = gpc_params

    @abstractmethod
    def execute(self):
        raise NotImplementedError

    @property
    def executors(self) -> List[ChangeExecutor]:
        return []

    def _echo_report_header(self, header_content):
        click.secho(ChangeSetting.HORIZONTAL_DOUBLEBAR)
        click.secho(header_content, fg="magenta", bold=True)
        click.secho(ChangeSetting.HORIZONTAL_DOUBLEBAR)

    def _echo_diff_report(self):
        click.echo(self._changes_to_string())

    def _changes_to_string(self):
        if self.gpc_params.diff and not self.has_changes():
            return "No changes found."
        changes_str = ChangeSetting.HORIZONTAL_BAR + "\n"
        changes_str += (
            ChangeSetting.get_line_header(prefix="  ") + ChangeSetting.HORIZONTAL_BAR + "\n"
        )
        for value in self.all_change_properties():
            if not self.gpc_params.diff or value.has_diff():
                changes_str += value.indented(prefix="  ")
                changes_str += ChangeSetting.HORIZONTAL_BAR + "\n"
        changes_str += ChangeSetting.HORIZONTAL_DOUBLEBAR
        return changes_str

    @property
    def status(self):
        return SUCCESS if all(x.success for x in self.executors) else FAIL

    @property
    def errors(self):
        errors = []
        for executor in self.executors:
            errors += executor.errors
        return errors

    def save(self):
        """Save settings."""
        self._echo_diff_report()
        if self.gpc_params.mode == RunMode.INTERACTIVE:
            if self._do_you_update():
                self._apply_changes()
        elif self.gpc_params.mode == RunMode.APPLY:
            self._apply_changes()
        else:
            click.secho("Dry run mode: No change applied", fg="yellow")

    @staticmethod
    def _do_you_update():
        choice = click.prompt(
            "Do you want update these changes?",
            default="y",
            type=click.Choice(["y", "n"]),
        )
        return choice == "y"

    def get_changes_json(self):
        return sorted(
            (c.to_dict() for c in self.all_change_properties()),
            key=lambda x: x["property_name"],
        )

    def get_diff_json(self):
        diff = {}
        for c in self.all_change_properties():
            if c.has_diff():
                cd = c.diff_to_dict()
                diff[cd["property_name"]] = cd
        return diff

    def _apply_changes(self):
        click.echo("Applying changes...")
        for executor in self.executors:
            executor.apply()
        click.secho("Changes applied")

    def all_change_properties(self):
        values = []
        for executor in self.executors:
            for change in executor.changes:
                values.append(change)
        return values

    def has_changes(self):
        for change in self.all_change_properties():
            if change.has_diff():
                return True
        return False

    def update_settings(self):
        """Update all settings of gitlab object."""
        click.echo("Updating settings...")
        # List of the users and groups which will members of project/groups.
        members_user = []  # type: List[int]
        members_group = []  # type: List[str]
        for executor in self.executors:
            executor.update(self.gpc_params.mode, members_user, members_group)

    def get_report(self):
        report = {
            "status": self.status,
            "rule": self.rule,
            "updated": self.has_changes(),
            "changes": self.get_changes_json(),
            "diff": self.get_diff_json(),
        }
        if self.status == FAIL:
            report["errors"] = self.errors
        return report


class GroupRuleExecutor(RuleExecutor):
    def __init__(
        self,
        gl: Gitlab,
        group_path: str,
        rule: GroupRule,
        gpc_params: GpcParameters,
    ):
        super().__init__(gl, rule, gpc_params)
        self.group_path = group_path

    @cachedproperty
    def group(self):
        return self.gitlab.groups.get(self.group_path)

    # pylint: disable=invalid-overridden-method
    @cachedproperty
    def executors(self) -> List[ChangeExecutor]:  # type: ignore
        factory = ChangeExecutorsFactory()
        return factory.init_executors(
            gl=self.gitlab,
            item_path=self.group_path,
            item=self.group,
            rule=self.rule,
            gpc_params=self.gpc_params,
        )

    # pylint: enable=invalid-overridden-method

    def _echo_diff_report(self):
        click.echo("Change for group:")
        super()._echo_diff_report()

    def execute(self):
        self._echo_report_header(
            "Policy for    : {group}\n"
            "URL           : {group_url}\n"
            "Rule name     : {rule_name}\n"
            "Custom rules  : {rule_override}".format(
                group=self.group_path,
                group_url=self.group.web_url,
                rule_name=self.rule.get("rule_name", "N/A"),
                rule_override="yes" if self.rule.get("custom_rules", None) else "no",
            )
        )
        log.info(
            "Configuring group...",
            configurator=self.gpc_params.config_project_url,
            mode=self.gpc_params.mode,
            group=self.group_path,
        )
        self.update_settings()
        self.save()
        log.info(
            "Group configured.",
            configurator=self.gpc_params.config_project_url,
            mode=self.gpc_params.mode,
            project=self.group_path,
        )
        return self.status == SUCCESS

    def get_report(self):
        report = super().get_report()
        report["group_name"] = self.group_path
        return report


class ProjectRuleExecutor(RuleExecutor):
    def __init__(
        self,
        gl: Gitlab,
        project_path: ProjectName,
        rule: ProjectRule,
        gpc_params: GpcParameters,
    ):
        super().__init__(gl, rule, gpc_params)
        self.project_path = project_path

    @cachedproperty
    def project(self):
        return self.gitlab.projects.get(self.project_path)

    # pylint: disable=invalid-overridden-method
    @cachedproperty
    def executors(self) -> List[ChangeExecutor]:  # type: ignore
        factory = ChangeExecutorsFactory()
        return factory.init_executors(
            gl=self.gitlab,
            item_path=self.project_path,
            item=self.project,
            rule=self.rule,
            gpc_params=self.gpc_params,
        )

    # pylint: enable=invalid-overridden-method

    def _echo_diff_report(self):
        click.echo("Change for project:")
        super()._echo_diff_report()

    def execute(self):
        self._echo_report_header(
            "Policy for    : {project}\n"
            "URL           : {project_url}\n"
            "Rule name     : {rule_name}\n"
            "Custom rules  : {rule_override}".format(
                project=self.project_path,
                project_url=self.project.web_url,
                rule_name=self.rule.get("rule_name", "N/A"),
                rule_override="yes" if self.rule.get("custom_rules", None) else "no",
            )
        )
        log.info(
            "Configuring project...",
            configurator=self.gpc_params.config_project_url,
            mode=self.gpc_params.mode,
            project=self.project_path,
        )
        self.update_settings()
        self.save()
        log.info(
            "Project configured.",
            configurator=self.gpc_params.config_project_url,
            mode=self.gpc_params.mode,
            project=self.project_path,
        )
        return self.status == SUCCESS

    def get_report(self):
        report = super().get_report()
        report["project_name"] = self.project_path
        return report
