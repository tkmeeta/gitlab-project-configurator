# coding: utf-8

# Third Party Libraries
from gitlab.v4.objects import Project


class ProjectApproval:

    """
    Wrapper class to manage project approvals which is managed by 2 apis.

    https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-configuration

    /projects/:id/approvals: manage globals configuration.
    /projects/:id/approval_rules: allow to create rules to define list
    of users and groups which will act as approvers
    (only one rule allowed for our gitlab instance).
    """

    def __init__(self, project: Project):
        self._approvals_manager = project.approvals.get()
        # Try to get the first approval rules (Gitlab EE only allow one)
        self._approvalrules_manager = project.approvalrules
        self._approvalrules_config = None

    @property
    def reset_approvals_on_push(self):
        return self._approvals_manager.reset_approvals_on_push

    @reset_approvals_on_push.setter
    def reset_approvals_on_push(self, value):
        self._approvals_manager.reset_approvals_on_push = value

    @property
    def disable_overriding_approvers_per_merge_request(self):
        return self._approvals_manager.disable_overriding_approvers_per_merge_request

    @disable_overriding_approvers_per_merge_request.setter
    def disable_overriding_approvers_per_merge_request(self, value):
        self._approvals_manager.disable_overriding_approvers_per_merge_request = value

    @property
    def merge_requests_author_approval(self):
        return self._approvals_manager.merge_requests_author_approval

    @merge_requests_author_approval.setter
    def merge_requests_author_approval(self, value):
        self._approvals_manager.merge_requests_author_approval = value

    @property
    def enable_committers_approvers(self):
        return not self._approvals_manager.merge_requests_disable_committers_approval

    @enable_committers_approvers.setter
    def enable_committers_approvers(self, value):
        self._approvals_manager.merge_requests_disable_committers_approval = not value

    @property
    def merge_requests_disable_committers_approval(self):
        return self._approvals_manager.merge_requests_disable_committers_approval

    @merge_requests_disable_committers_approval.setter
    def merge_requests_disable_committers_approval(self, value):
        self._approvals_manager.merge_requests_disable_committers_approval = value

    @property
    def approval_rules(self):
        if self._approvalrules_config is None:
            self._approvalrules_config = (self._approvalrules_manager.list() or [None])[0]
        return self._approvalrules_config

    @property
    def approver_groups(self):
        if self.approval_rules:
            return self.approval_rules.groups
        return []

    @property
    def approvers(self):
        if self.approval_rules:
            return self.approval_rules.users
        return []

    @property
    def approvals_before_merge(self):
        if self.approval_rules:
            return self.approval_rules.approvals_required
        return 0

    def set_approvers(
        self,
        name="Default",
        approver_ids=None,
        approver_group_ids=None,
        approvals_required=0,
    ):
        config = {
            "name": name,
            "approvals_required": approvals_required,
        }
        if approver_ids:
            config["user_ids"] = approver_ids
        if approver_group_ids:
            config["group_ids"] = approver_group_ids

        # Due to issue in approval rules API
        # https://gitlab.com/gitlab-org/gitlab/-/issues/211665
        # Approval rules is not correctly updated when users/groups are empty
        # To be sure that it is updated we delete the current one and recreate a new one
        if self.approval_rules:
            self._approvalrules_manager.delete(self.approval_rules.id)
            self._approvalrules_config = None

        self._approvalrules_manager.create(config)

    def save(self):
        self._approvals_manager.save()
