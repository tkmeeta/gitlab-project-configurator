# coding: utf-8

# Standard Library
from typing import Any
from typing import Generator
from typing import List
from typing import Optional
from typing import Union

# Third Party Libraries
import attr


Url = str
ProjectName = str
RuleName = str
ProjectRule = Any
GroupRule = Any
Rule = Any
Something = Any


@attr.s
class ProjectPathRule:
    @property
    def project_path(self):
        # To avoid cyclic import, set the import in the getter
        # pylint: disable=import-outside-toplevel

        # Gitlab-Project-Configurator Modules
        from gpc.helpers.gitlab_helper import clean_gitlab_project_name

        # pylint: enable=import-outside-toplevel
        return clean_gitlab_project_name(self._project_path)

    _project_path = attr.ib()  # type: ignore
    rule = attr.ib()  # type: ProjectRule
    recursive = attr.ib()  # type: bool
    not_seen_yet_only = attr.ib()  # type: bool
    excludes = attr.ib(default=None)  # type: Optional[List[str]]


@attr.s
class GroupPathRule:
    group_path = attr.ib()  # type: ignore
    rule = attr.ib()  # type: GroupRule


GenProjectPathRules = Generator[ProjectPathRule, None, None]
GenGroupPathRules = Generator[GroupPathRule, None, None]
OptionalProjectNameList = Optional[List[ProjectName]]
OptionalRuleName = Optional[RuleName]


def listify(c: Union[List[Something], Something]) -> List[Something]:
    if not isinstance(c, list):
        return [c]
    return c
