# coding: utf-8

GPC_ERR_CODE_SUCCESS = 0
GPC_ERROR_CODE_FAILURE = 1
GPC_ERR_CODE_PROJECT_FAILURE = 10
GPC_ERR_VALIDATION_ERROR = 20
GPC_ERR_SCHEMA_ERROR = 30
GPC_ERR_PROFILE_NO_EXIST = 40
GPC_ERR_VARIABLES = 50
GPC_ERR_MEMBER = 60
GPC_USER_ERR = 70
GPC_ERR_LABEL = 80
