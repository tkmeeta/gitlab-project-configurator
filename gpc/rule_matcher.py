# coding: utf-8

"""
Matches which rules should apply on which project.
"""

# Standard Library
from abc import ABC
from abc import abstractmethod
from typing import Optional

# Third Party Libraries
from dictns import Namespace
from gitlab import Gitlab
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.helpers.remerge import ListOverrideBehavior
from gpc.helpers.remerge import remerge
from gpc.helpers.types import GenGroupPathRules
from gpc.helpers.types import GenProjectPathRules
from gpc.helpers.types import GroupPathRule
from gpc.helpers.types import ProjectPathRule
from gpc.helpers.types import Rule


log = get_logger()

# pylint: disable=assignment-from-none, useless-return


class RuleMatcher(ABC):

    """
    I am responsible for associating projects with rules.

    I find in the configuration which policies should be applied on which projects.
    Note projects can be a simple group name, the unrolling of every project in this
    group is not under my responsibility.

    I support the rule override.
    """

    def __init__(self, gitlab: Gitlab, raw_config: Namespace):
        self.raw_config = raw_config
        self.gitlab = gitlab

    @property
    def rules(self):
        return []

    @abstractmethod
    def find_rules(self):
        raise NotImplementedError()

    def get_named_rule(self, rule_name: str) -> Optional[Rule]:
        log.debug("searching named rule", rule_name=rule_name)
        for item_rule in self.rules:
            if item_rule.rule_name == rule_name:
                return self._inject_profiles(item_rule)
        return None

    def _prepare_rule(self, rule: Rule, item_cfg: Namespace) -> Rule:
        rule = self._handle_rule_inheritance(rule)
        item_rule = self._handle_rule_override(rule, item_cfg)
        return item_rule

    def _handle_rule_inheritance(self, rule: Rule) -> Rule:
        if "inherits_from" not in rule:
            return rule
        irule = Namespace(rule.copy())
        wanted_rule_name = irule.inherits_from
        for other_rule in self.rules:
            if other_rule.rule_name == wanted_rule_name:
                log.debug(
                    "Applying rule inheritance",
                    rule_name=irule.rule_name,
                    other_rule_name=other_rule.rule_name,
                )
                # Apply recursive inherits_from
                other_rule = self._handle_rule_inheritance(other_rule.copy())
                del other_rule["rule_name"]
                irule = self._override_rule(other_rule, irule)
                return irule
        raise NotImplementedError(
            "This case 'invalid inherits_from' should not happen, "
            "the validator should have caught it"
        )

    def _handle_rule_override(self, rule: Rule, item_cfg: Namespace) -> Rule:
        if "custom_rules" not in item_cfg:
            return rule
        crule = Namespace(rule.copy())
        custom_rules = item_cfg["custom_rules"]
        if custom_rules:
            log.debug(
                "Applying custom rules",
                rule_name=rule.get("rule_name", "?"),
                custom_rules=custom_rules,
            )
            crule = self._override_rule(crule, custom_rules)
        return crule

    def _inject_profiles(self, rule: Rule) -> Rule:
        cr = Namespace(rule.copy())
        for profiles in ["variable_profiles", "member_profiles", "label_profiles"]:
            if profiles not in self.raw_config:
                continue

            cr[profiles] = self.raw_config[profiles]
        return cr

    def _override_rule(self, rule: Rule, other_rule: Rule) -> Rule:
        return Namespace(
            remerge([rule, other_rule], list_update_behavior=ListOverrideBehavior.REPLACE)
        )


class GroupRuleMatcher(RuleMatcher):
    @property
    def rules(self):
        return self.raw_config.get("groups_rules")

    def find_rules(self) -> GenGroupPathRules:
        log.debug("Listing group from configuration")
        for group_cfg in self.raw_config.get("groups_configuration", {}):
            for grp_path in group_cfg.get("paths", []):
                rule = self.get_named_rule(group_cfg.rule_name)
                if not rule:
                    raise ValueError("Cannot find rule name: {0}".format(group_cfg.rule_name))
                log.debug("Found path from configuration", path=grp_path)
                yield GroupPathRule(
                    group_path=grp_path,
                    rule=self._prepare_rule(rule=rule, item_cfg=group_cfg),
                )


class ProjectRuleMatcher(RuleMatcher):
    @property
    def rules(self):
        return self.raw_config.get("projects_rules")

    def find_rules(self) -> GenProjectPathRules:
        log.debug("Listing project from configuration")
        for project_cfg in self.raw_config.get("projects_configuration", {}):
            for proj_path in project_cfg.get("paths", []):
                rule = self.get_named_rule(project_cfg.rule_name)
                if not rule:
                    raise ValueError("Cannot find rule name: {0}".format(project_cfg.rule_name))
                log.debug("Found path from configuration", path=proj_path)
                yield ProjectPathRule(
                    project_path=proj_path,
                    rule=self._prepare_rule(rule=rule, item_cfg=project_cfg),
                    recursive=project_cfg.get("recursive", False),
                    excludes=project_cfg.get("excludes", None),
                    not_seen_yet_only=project_cfg.get("not_seen_yet_only", False),
                )
