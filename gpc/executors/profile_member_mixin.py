# coding: utf-8

"""
Mixin to extract members of profiles section.
"""
# Third Party Libraries
import attr

from gitlab import exceptions as gl_exceptions
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.helpers.exceptions import GpcMemberError
from gpc.helpers.exceptions import GpcUserError
from gpc.helpers.gitlab_helper import get_group
from gpc.helpers.gitlab_helper import get_user_by_username


log = get_logger()


@attr.s
class GPCMember:

    name = attr.ib()  # type: str
    gl_id = attr.ib()  # type: int


class GPCUser(GPCMember):
    pass


@attr.s
class GPCGroup(GPCMember):
    full_path = attr.ib()  # type: str


class ProfileMemberMixin:
    def get_merged_profiles(self, profiles):
        merged_profiles = []
        for profile_name in profiles:
            log.debug("Injecting profile from : {0}".format(profile_name))
            profile = self.get_member_profile(profile_name)
            merged_profiles += profile.members
        return merged_profiles

    def get_member_profile(self, profile_name):
        if not self.rule.get("member_profiles"):
            raise GpcMemberError(
                "ERROR on project {0}: "
                "The import of member_profiles profile {1} is impossible, because"
                "the section 'member_profiles' does not exist.".format(
                    self.project_path, profile_name
                )
            )
        for member_profile in self.rule.get("member_profiles"):
            if member_profile.name == profile_name:
                return member_profile
        raise GpcMemberError(
            "ERROR on project {0}: "
            "The import of member profile {1} is impossible, because"
            "this profile name is not found in the 'member_profiles' "
            "section.".format(self.project_path, profile_name)
        )

    def get_member(self, member_name):
        try:
            gl_user = get_user_by_username(self.gitlab, member_name)
            return GPCUser(name=member_name, gl_id=gl_user.id)
        except GpcUserError:
            pass
        try:
            group = get_group(self.gitlab, member_name)
            return GPCGroup(name=group.name, gl_id=group.id, full_path=group.full_path)
        except gl_exceptions.GitlabGetError:
            raise GpcMemberError("The username or group name {} does not exist".format(member_name))
