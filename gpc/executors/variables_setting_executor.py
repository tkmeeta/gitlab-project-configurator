# coding: utf-8

"""
Make the update of environment variable.
"""
# Standard Library
import re

from os import environ
from typing import Generator

# Third Party Libraries
import attr
import click

from boltons.cacheutils import cachedproperty
from dictns import Namespace
from gitlab.exceptions import GitlabListError
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.change_setting import ChangePropertySetting
from gpc.executors.properties_updator import ChangePropertyExecutor
from gpc.helpers.exceptions import GpcPermissionError
from gpc.helpers.exceptions import GpcVariableError
from gpc.helpers.hider import hide_value
from gpc.parameters import RunMode
from gpc.property_manager import PropertyBean
from gpc.property_manager import PropertyManager


log = get_logger()

REGEX_MASKED_VARIABLE = r"^[a-zA-Z0-9+/\-_=:@]{8,}\Z"


@attr.s
class ProjectVariable(PropertyBean):

    protected = attr.ib()  # type: bool
    value = attr.ib()  # type: str
    is_hidden = attr.ib(default=False)  # type: bool
    warning_msg = attr.ib(default="")  # type: str
    variable_type = attr.ib(default="env_var")  # type: str
    masked = attr.ib(default=False)  # type: bool

    @staticmethod
    def to_project_variables(api_variables):
        project_variables = []
        for api_variable in api_variables:
            project_variables.append(ProjectVariable.to_project_variable(api_variable))
        return project_variables

    @staticmethod
    def to_project_variable(api_variable):
        protected = api_variable.protected if hasattr(api_variable, "protected") else False
        variable_type = (
            api_variable.variable_type if hasattr(api_variable, "variable_type") else "env_var"
        )
        masked = api_variable.masked if hasattr(api_variable, "masked") else False
        return ProjectVariable(
            name=api_variable.key,
            protected=protected,
            value=api_variable.value,
            variable_type=variable_type,
            masked=masked,
        )

    @cachedproperty
    def value_hidden(self):
        return hide_value(self.value)

    def get_query(self):
        return {
            "key": self.name,
            "protected": self.protected,
            "value": self.value,
            "variable_type": self.variable_type,
            "masked": self.masked,
        }

    def to_dict(self):
        dict_variable = {
            "name": self.name,
            "protected": self.protected,
            "warning": self.warning_msg,
            "variable_type": self.variable_type,
            "masked": self.masked,
        }
        if self.is_hidden:
            dict_variable["value"] = self.value_hidden
        else:
            dict_variable["value"] = self.value
        return dict_variable


class ChangeVariables(ChangePropertySetting):

    sub_properties = ["protected", "value", "variable_type", "masked"]
    status_to_process = ["removed", "updated", "kept", "added", "warning"]

    @cachedproperty
    def differences(self):
        before_properties = {prop.name: prop for prop in self.before}
        after_properties = {prop.name: prop for prop in self.after}
        differences = {}

        for name, prop in before_properties.items():
            status = "kept"
            after_prop = None

            if name in after_properties:
                after_prop = after_properties[name].to_dict()
                prop.is_hidden = after_properties[name].is_hidden
                if after_properties[name].warning_msg:
                    status = "warning"
                    prop.is_hidden = True
                elif after_properties[name].value is None:
                    # Remove the variable if value is set to None
                    prop.is_hidden = prop.protected
                    status = "removed"
                elif prop != after_properties[name]:
                    status = "updated"
            elif self.keep_existing:
                # Old variable but user wants to keep it
                prop.is_hidden = prop.protected
                status = "kept"
                after_prop = prop.to_dict()
            else:
                # Old variable, delete it
                prop.is_hidden = prop.protected
                status = "removed"
            differences[name] = {
                "status": status,
                "before": prop.to_dict(),
                "after": after_prop,
            }

        for name, prop in after_properties.items():
            status = "warning" if prop.warning_msg else "added"
            if name not in differences.keys():
                differences[name] = {
                    "status": status,
                    "before": None,
                    "after": prop.to_dict(),
                }

        return differences

    def _build_str_by_status(self, after, before, name, status, to_str):
        if status == "warning":
            bef_protected = before.get("protected") if before else None
            bef_value = before.get("value") if before else None
            to_str += self.FMT.format(
                property_name="      name",
                before=name,
                after=after.get("warning"),
                action=status,
            )
            to_str += (
                self.REF_PROPERTY.format(
                    sub_prop="      protected", before=str(bef_protected), after="None"
                )
                + "\n"
            )
            to_str += (
                self.REF_PROPERTY.format(
                    sub_prop="      value", before=str(bef_value), after="None"
                )
                + "\n"
            )
        else:
            to_str = super()._build_str_by_status(after, before, name, status, to_str)
        return to_str


class VariablesSettingExecutor(ChangePropertyExecutor):

    order = 40
    applicable_to = ["group", "project"]
    sections = ["variables"]

    @property
    def variables(self):
        if "variables" not in self.rule or self.rule.variables is None:
            return None
        return self.rule.variables

    def _apply(self):
        if self.changes:
            variables = self.changes[0]
            self._save_properties(
                PropertyManager(self.project.variables), variables, variables.after
            )

    def _update(self, mode: RunMode, members_user, members_group):
        if "variables" not in self.rule or self.rule.variables is None:
            return

        keep_existing_variables = self.rule.get("keep_existing_variables", False)
        previous_variables = ProjectVariable.to_project_variables(
            self.project.variables.list(as_list=False)
        )

        preparator = VariablesSettingPreparator(self.project_path, self.rule, self.rule.variables)
        env_variables = preparator.prepare_variables(mode)

        try:
            self.changes.append(
                ChangeVariables(
                    property_name="variables",
                    before=previous_variables,
                    after=env_variables,
                    show_diff_only=self.show_diff_only,
                    keep_existing=keep_existing_variables,
                )
            )
        except GitlabListError as e:
            # Check if pipeline is enabled
            if e.response_code == 403 and not self.project.jobs_enabled:
                error_message = (
                    "ERROR on project {0}: Environment variables can not be set. "
                    "Please ensure Pipelines are enabled "
                    "on your project".format(self.project_path)
                )
                raise GpcPermissionError(error_message)
            raise


class VariablesSettingPreparator:
    def __init__(self, project_path, rule, variables):
        self.rule = rule
        self.variables = variables
        self.project_path = project_path

    def prepare_variables(self, mode):
        env_variables = []

        for env_variable in self._expand_variables():
            name = env_variable.name
            variable_type = (
                env_variable.variable_type if hasattr(env_variable, "variable_type") else "env_var"
            )
            protected = self.is_protected_variable(env_variable)
            value = env_variable.get("value", None)
            masked = env_variable.get("masked", False)
            value_from_envvar = env_variable.get("value_from_envvar", None)
            is_hidden = False
            is_hidden, value, warning_msg = self._extract_value(
                is_hidden, mode, value, value_from_envvar
            )
            if masked and value:
                VariablesSettingPreparator.validate_value(name, value)

            env_variables.append(
                ProjectVariable(
                    name=name,
                    protected=protected,
                    value=value,
                    is_hidden=is_hidden,
                    variable_type=variable_type,
                    masked=masked,
                    warning_msg=warning_msg,
                )
            )
        return env_variables

    def is_protected_variable(self, env_variable):
        return env_variable.get("protected", False)

    def _extract_value(self, is_hidden, mode, value, value_from_envvar):
        warning_msg = ""
        if value_from_envvar:
            if environ.get(value_from_envvar) is not None:
                value = environ.get(value_from_envvar)
                is_hidden = True
            else:
                warning_msg = "/!\\ Environment variable {0} not found.".format(value_from_envvar)
                if mode is RunMode.DRY_RUN:
                    click.secho(warning_msg, fg="red")
                    click.secho(
                        "/!\\ In Apply or Interactive mode" "your configuration will fail.",
                        fg="yellow",
                    )
                else:
                    raise ValueError(warning_msg)
        return is_hidden, value, warning_msg

    def _expand_variables(self) -> Generator[Namespace, None, None]:
        """I inject the variable_profiles when applicable."""
        for variable in self.variables:
            if "import" in variable:
                var_profile_name = variable.get("import")
                log.debug("Injecting variable profile from : {0}".format(var_profile_name))
                if not self.rule.get("variable_profiles") or not self.rule.get(
                    "variable_profiles", {}
                ).get(var_profile_name):
                    raise GpcVariableError(
                        "On project {0}: "
                        "The import of variable {1} is impossible, because"
                        "this variable is not found "
                        "in the 'variable_profiles' section.".format(
                            self.project_path, var_profile_name
                        )
                    )
                for rv in self.rule.get("variable_profiles", {}).get(var_profile_name, []):
                    yield rv
            else:
                yield variable

    @staticmethod
    def validate_value(name, value):
        if not re.match(REGEX_MASKED_VARIABLE, value):
            raise GpcVariableError(
                f"The '{name}' value does not respect the requirements"
                " for masked variable. See the requirements here: "
                "https://docs.gitlab.com/ee/ci/variables/README.html"
                "#masked-variables"
            )
