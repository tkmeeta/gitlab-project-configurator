# coding: utf-8

"""
Change executor abstract class.
"""

# Third Party Libraries
import click

from gitlab.exceptions import GitlabDeleteError

# Gitlab-Project-Configurator Modules
from gpc.executors.change_executor import ChangeExecutor


# pylint: disable= abstract-method


class PropertyUpdatorMixin:
    def _save_properties(self, manager, change_properties, properties):
        for name in change_properties.remove:
            try:
                manager.rm_existing(name)
            except GitlabDeleteError as e:
                click.secho("ERROR: {}".format(str(e.error_message)), fg="red")
        # target to update or create
        variables_to_cu = change_properties.update_or_create
        for variable in properties:
            if variable.name in variables_to_cu:
                manager.create(variable, self.project_path)


class ChangePropertyExecutor(ChangeExecutor, PropertyUpdatorMixin):
    pass
